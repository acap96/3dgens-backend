# Strapi application

A quick description of your strapi application


## How to deploy

### For staging

1. Build docker image

```
./script/buildDockerImage.sh <VersionNumber>-staging
```

2. Create container

```
docker run -e NODE_ENV=staging registry.gitlab.com/acap96/3dgens-backend:<VersionNumber>-staging
```

### For production

1. Build docker image

```
./script/buildDockerImage.sh <VersionNumber>-production
```

2. Create container

```
docker run -e NODE_ENV=staging registry.gitlab.com/acap96/3dgens-backend:<VersionNumber>-production
```
