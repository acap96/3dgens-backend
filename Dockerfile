FROM node:10.16.3-jessie

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./package.json /usr/src/app

RUN npm install && npm cache clean --force

COPY ./ /usr/src/app

CMD ["npm", "start"]
