#!/bin/bash

if [ -z "$1" ]
then
    echo "Version is not specified"
    echo "./scripts/buildContainer.sh VERSION_NUM"
    exit 1
else
    echo "Build lucid-backend container version $1"
    if [[ $1 == *"staging"* ]]; then 
        NODE_ENV=staging npm run build
    elif [[ $1 == *"production"* ]]; then
        NODE_ENV=production npm run build
    else
        npm run build
    fi
    docker build -t registry.gitlab.com/acap96/3dgens-backend:$1 -t registry.gitlab.com/acap96/3dgens-backend:latest .
    docker push registry.gitlab.com/acap96/3dgens-backend:$1
    docker push registry.gitlab.com/acap96/3dgens-backend:latest
fi
